# GameCompanionApp
## A Companion application to an imaginary racing game. 
### This app was developed as an assignment with 3 students.
The app accesses a database with statistics of the imaginary players which is shown in forms of graphs and diagrams to the user. It includes functionality to search and lookup multiple players, which gets stored in different swipe-able tabs for easy comparisons between them. Login functionality is also possible, where one can change the profile picture, and profile description of the logged in user.

## Screenshots
### Login
![Imgur](https://i.imgur.com/AyxCBSX.png)
### Logged in
![Imgur](https://i.imgur.com/kM64j8S.png)
![Imgur](https://i.imgur.com/zFi5Uzm.png)
![Imgur](https://i.imgur.com/Yic1Vnq.png)
### Search and add user to compare (+ button)
![Imgur](https://i.imgur.com/z8jU4BX.png)
![Imgur](https://i.imgur.com/zX9Fbxs.png)
![Imgur](https://i.imgur.com/mRqm9xo.png)
### Comparing users
![Imgur](https://i.imgur.com/OSd8pLV.png)