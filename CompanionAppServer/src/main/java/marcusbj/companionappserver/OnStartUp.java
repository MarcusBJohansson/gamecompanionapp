/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package marcusbj.companionappserver;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.enterprise.identitystore.PasswordHash;
import javax.ws.rs.core.Response;

/**
 * @since Dec 6, 2018
 * @version 1.0
 * @author Marcus Benjamin Johansson
 */
@Singleton
@Startup
public class OnStartUp {
    @PersistenceContext
    EntityManager em;
    
//    @Inject
//    CompanionAppService appService;
    
    @Inject
    PasswordHash hasher;
    
    @PostConstruct
    public void init(){
        System.out.println("Generating dummydata..");
        addDummyData();
//        appService.addDummyData();
    }
    
    public Response addDummyData() {

        String[] testNames1 = {"Toby", "Mike", "Donald", "Dmitri", "Charlotte", "Maxwell", "Chad", "Bruno", "Sasha", "Tyler", "Brigsby", "Jared"};
        String[] testNames2 = {"Mars", "Trump", "James", "Jackson", "Willburry", "Jones", "Bond", "Baron", "Chrysler", "Jobs", "Huckaby", "Leto"};

        List<Player> newPlayers = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            int randomIndex1 = (int) (Math.random() * testNames1.length);
            int randomIndex2 = (int) (Math.random() * testNames2.length);
            String defaultPwd = "123";
            Player newPlayer = Player.CreateNewPlayer("e" + i + "@d.com", hasher.generate(defaultPwd.toCharArray()), testNames1[randomIndex1] + " " + testNames2[randomIndex2]);
            newPlayer.setHighestAlcohol((float) (Math.random() * 6));
            newPlayer.setLongestDrift((float) (Math.random() * 50));
            newPlayer.incrementNumberOfTakedowns((int) (Math.random() * 100));
            newPlayer.incrementNumberOfTinderMatches((int) (Math.random() * 50));
            newPlayer.incrementTimesOutOfBounds((int) (Math.random() * 250));
            newPlayers.add(newPlayer);
            em.persist(newPlayer);
        }
        int day = 1;
        for (int i = 0; i < 40; i++) {
            int numberOfLaps = (int) (Math.random() * 3) + 3;
            int mapIndex = (int) (Math.random() * 4);
            day = Math.random() > 0.5f ? day + 1 : day;
            Match match = new Match(mapIndex, numberOfLaps, LocalDateTime.of(2018, 8, day, (int) (Math.random() * 24), (int) (Math.random() * 60), (int) (Math.random() * 60)));
            Set<MatchResult> results = new HashSet<>();
            Set<Player> participants = new HashSet<>();
            for (int j = 0; j < 10; j++) {
                Player randomPlayer = null;
                while (randomPlayer == null || participants.contains(randomPlayer)) {
                    int randomIndex = (int) (Math.random() * newPlayers.size());
                    randomPlayer = newPlayers.get(randomIndex);
                }
                participants.add(randomPlayer);
                MatchResult.PlayableCharacter randomCharacter = MatchResult.PlayableCharacter.values()[(int) (Math.random() * MatchResult.PlayableCharacter.values().length)];
                MatchResult playerResult = new MatchResult(match, (float) (Math.random() * numberOfLaps * 50000 + 50000), randomCharacter);
                playerResult.setUpPlayer(randomPlayer);
                results.add(playerResult);
                em.persist(playerResult);
                em.merge(randomPlayer);

            }
            match.setPlayerResults(results);
            computeRankDistribution(match);
            em.persist(match);
        }

        return Response.ok("dummydata created").build();
    }

    private void computeRankDistribution(Match match) {
        ArrayList<MatchResult> playerPlaceInRace = new ArrayList<>(match.getPlayerResults());
        Collections.sort(playerPlaceInRace, new Comparator<MatchResult>() {
            @Override
            public int compare(MatchResult r1, MatchResult r2) {
                return r1.getLapTime() < r2.getLapTime() ? -1 : (r1.getLapTime() > r2.getLapTime()) ? 1 : 0;
            }
        });

        for (int i = 0; i < playerPlaceInRace.size(); i++) {
            MatchResult currentMatchResult = playerPlaceInRace.get(i);
            float placeSign = (playerPlaceInRace.size() / 2f - i - 0.5f) / (playerPlaceInRace.size() / 2f);

            float rankMultiplier = 1;//(float) /*Math.pow(*/ currentMatchResult.getPlayer().getRankPoints() / match.getAvearageRank()/*, 1.25f)*/;

            float rankDelta = 50;

            if (placeSign > 0) { //player gains rankpoints
                rankDelta *= (placeSign / rankMultiplier);
            }
            else { //player loses rank points
                rankDelta *= (placeSign * rankMultiplier);
            }

            int playerRankDelta = Math.round(rankDelta);
            currentMatchResult.setPlaceInMatch(i);
            currentMatchResult.setRankDelta(playerRankDelta);

//             if(i == 4) System.out.println("match player count: " + playerPlaceInRace.size() + "    ");
//            if(i == 4) System.out.println("fourth place rank: " + currentMatchResult.getPlayer().getRankPoints() + " vs avg: " + match.getAvearageRank() + "  rank multiplier: " + rankMultiplier + " place sign: " + placeSign + "   rankdelta: " + rankDelta);
            em.merge(currentMatchResult);
            em.merge(currentMatchResult.getPlayer());

        }
    }

}
