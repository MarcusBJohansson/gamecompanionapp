/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marcusbj.companionappserver;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @since Nov 14, 2018
 * @version 1.0
 * @author Marcus Benjamin Johansson
 */
@Entity
public class Player implements Serializable {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Id
    private String id;
    private String username;
    private String password;
    
    @Column(name = "ROLE_NAME")
    private String role;

    private int rankPoints;
    private List<Integer> placeCount;
    private float longestDrift;
    private int timesOutOfBounds;
    private float highestAlcohol;
    private int numberOfTakedowns;
    private int numberOfTinderMatches;
    private String description;
    private String profilePicturePath;

    public String getProfilePicturePath() {
        return profilePicturePath;
    }

    public void setProfilePicturePath(String profilePicturePath) {
        this.profilePicturePath = profilePicturePath;
    }

    @OneToMany(mappedBy = "player")
    private ArrayList<MatchResult> matchHistory;

    private Player(String id, String password, String username) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = "user";
        placeCount = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            placeCount.add(0);
        }
        
        matchHistory = new ArrayList<>();

        longestDrift = 0;
        timesOutOfBounds = 0;
        highestAlcohol = 0;
        numberOfTakedowns = 0;
        numberOfTinderMatches = 0;
        rankPoints = 150;
        description = "This is a description";
    }

    public Player() {
    }

    public int getRankPoints() {
        return rankPoints;
    }

    public void progressRank(int deltaRankPoints) {
        this.rankPoints += deltaRankPoints;
        if(this.rankPoints < 0){
            this.rankPoints = 0;
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Integer> getPlaceCount() {
        return placeCount;
    }

    public void incrementPlaceCount(int index) {
        while (index >= placeCount.size()) {
            placeCount.add(0);
        }
        placeCount.set(index, placeCount.get(index) + 1);
    }

    public float getLongestDrift() {
        return longestDrift;
    }

    public void setLongestDrift(float longestDrift) {
        if (longestDrift > this.longestDrift) {
            this.longestDrift = longestDrift;
        }
    }

    public int getTimesOutOfBounds() {
        return timesOutOfBounds;
    }

    public void incrementTimesOutOfBounds(int timesOutOfBounds) {
        this.timesOutOfBounds += timesOutOfBounds;
    }

    public float getHighestAlcohol() {
        return highestAlcohol;
    }

    public void setHighestAlcohol(float highestAlcohol) {
        if (highestAlcohol > this.highestAlcohol) {
            this.highestAlcohol = highestAlcohol;
        }
    }

    public int getNumberOfTakedowns() {
        return numberOfTakedowns;
    }

    public void incrementNumberOfTakedowns(int numberOfTakedowns) {
        this.numberOfTakedowns += numberOfTakedowns;
    }

    public int getNumberOfTinderMatches() {
        return numberOfTinderMatches;
    }

    public void incrementNumberOfTinderMatches(int numberOfTinderMatches) {
        this.numberOfTinderMatches = numberOfTinderMatches;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<MatchResult> getMatchHistory() {
        return matchHistory;
    }

    public void addMatchToHistory(MatchResult match) {
        this.matchHistory.add(match);
    }

    public static Player CreateNewPlayer(String email, String password, String username) {
        if (email.contains("@") && email.contains(".") && !username.isEmpty() && !password.isEmpty()) {
            return new Player(email, password, username);
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Player)) {
            return false;
        }
        Player other = (Player) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "id: " + id + " pwd: " + password + " username: " + username;
    }

}
