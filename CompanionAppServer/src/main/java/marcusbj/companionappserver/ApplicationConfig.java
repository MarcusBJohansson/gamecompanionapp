/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marcusbj.companionappserver;

import java.util.Set;
import javax.annotation.security.DeclareRoles;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import javax.security.enterprise.identitystore.PasswordHash;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 * @author Marcus Benjamin Johansson
 * @version 1.0
 * @since Oct 22, 2018
 */
@javax.ws.rs.ApplicationPath("api")
@DeclareRoles({"user"})
@DatabaseIdentityStoreDefinition(
        dataSourceLookup = "jdbc/GameStatsDB",
        callerQuery = "select password from player where id = ?",
        groupsQuery = "select role_name from player where id = ?",
        hashAlgorithm = PasswordHash.class)
@BasicAuthenticationMechanismDefinition
@ApplicationScoped
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(MultiPartFeature.class);
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(marcusbj.companionappserver.CompanionAppService.class);
    }

}
