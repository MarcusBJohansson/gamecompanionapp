/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marcusbj.companionappserver;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @since Nov 16, 2018
 * @version 1.0
 * @author Marcus Benjamin Johansson
 */
@Entity
@Table(name = "RACE_MATCH")
public class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    private Set<MatchResult> playerResults;

    private float avearageRank;

    private final int numberOfLaps;

    private final LocalDateTime dateTime;

    private final int racingMapId;

    public Match() {
        numberOfLaps = 0;
        dateTime = LocalDateTime.MIN;
        racingMapId = 0;
    }

    public Match(int racingMap, int numberOfLaps, LocalDateTime dateTime) {
        this.racingMapId = racingMap;
        this.numberOfLaps = numberOfLaps;
        this.dateTime = dateTime;
        avearageRank = 0;

    }

    public void setPlayerResults(Set<MatchResult> playerResults) {
        this.playerResults = playerResults;
        calcuateAvgRank();
    }

    public float getAvearageRank() {
        if(avearageRank == 0){
            calcuateAvgRank();
        }
        return avearageRank;
    }

    public Set<MatchResult> getPlayerResults() {
        return playerResults;
    }

    public int getRacingMapId() {
        return racingMapId;
    }

    public int getNumberOfLaps() {
        return numberOfLaps;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    private void calcuateAvgRank(){
        for (MatchResult r : playerResults) {
            avearageRank += r.getPlayer().getRankPoints();
        }
        avearageRank /= playerResults.size();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Match)) {
            return false;
        }
        Match other = (Match) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "marcusbj.companionappserver.Match[ id=" + id + " ]";
    }

}
