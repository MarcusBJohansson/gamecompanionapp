/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marcusbj.companionappserver;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @since Nov 14, 2018
 * @version 1.0
 * @author Marcus Benjamin Johansson
 */
@Entity
public class MatchResult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Player player;

    @ManyToOne
    private Match match;

    private float lapTime;

    private int rankDelta;

    private int placeInMatch;

    @Column(name = "CHARACTER_NAME")
    private String character;

    public MatchResult(Match match, float lapTime, PlayableCharacter character) {
        this.match = match;
        this.lapTime = lapTime;
        this.character = character.name();
        rankDelta = 0;
        placeInMatch = 0;
    }

    public MatchResult() {
    }

    public void setUpPlayer(Player player) {
        this.player = player;
        if (!player.getMatchHistory().contains(this)) {
            player.addMatchToHistory(this);
        }
    }

    public Long getId() {
        return id;
    }

    public int getPlaceInMatch() {
        return placeInMatch;
    }

    public String getCharacter() {
        return character;
    }

    public void setPlaceInMatch(int placeInMatch) {
        if (this.placeInMatch != 0) {
            return;
        }
        this.placeInMatch = placeInMatch;
        player.incrementPlaceCount(placeInMatch);
    }

    public int getRankDelta() {
        return rankDelta;
    }

    public void setRankDelta(int rankDelta) {
        if (this.rankDelta != 0) {
            return;
        }
        this.rankDelta = rankDelta;
        player.progressRank(rankDelta);
    }

    public Player getPlayer() {
        return player;
    }

    public Match getMatch() {
        return match;
    }

    public float getLapTime() {
        return lapTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MatchResult)) {
            return false;
        }
        MatchResult other = (MatchResult) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public enum PlayableCharacter {
        STORMI_WEBSTER,
        ESPEN_VAN_DANGO,
        MARCY_BJ,
        BEAR_NAR,
        KARAMBE,
        STONE_ISLAND_CHEESE_KING;
    }

    @Override
    public String toString() {
        return "marcusbj.companionappserver.GameMatch[ id=" + id + " ]";
    }

}
