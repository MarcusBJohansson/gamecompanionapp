/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marcusbj.companionappserver;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.security.enterprise.identitystore.PasswordHash;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Marcus Benjamin Johansson
 * @version 1.0
 * @since Oct 22, 2018
 */
@Stateless
@DeclareRoles({"user"})
@Path("stats")
public class CompanionAppService {

    @Context
    SecurityContext sc;

    @PersistenceContext
    EntityManager em;

    @Inject
    PasswordHash hasher;
    
    
    private String getImagePath() {
        return "D:\\Global repositories\\NTNU\\CompanionApp\\gamecompanionapp\\CompanionAppServer\\uploaded images";
    }
    
    private String getDefaultImagePath(){
        return "D:\\OneDrive\\ProfilePic\\default_avatar.jpg";
    }
    

    @Path("test")
    @RolesAllowed("user")
    @GET
    public String test(@Context SecurityContext seco) {
        return "you entered: " + seco.getUserPrincipal().getName();
    }

    @Path("check-pwd")
    @GET
    public Response CheckPassword(@QueryParam("email") String email, @QueryParam("pwd") String password) {
        Player p = em.find(Player.class, email);
        if (p == null) {
            return Response.noContent().build();
        }
        return Response.ok(hasher.verify(password.toCharArray(), p.getPassword())).build();
    }

    @Path("check")
    @GET
    public String checkUser() {
        String parameter = "e@d.com";
        TypedQuery<Player> q = em.createQuery("SELECT p FROM Player p WHERE p.id = :param", Player.class);
        q.setParameter("param", parameter);
        System.out.println(q.toString());
        return q.getSingleResult().toString();
    }

    @Path("login")
    @RolesAllowed("user")
    @GET
    public Response login() {
        Player player = em.find(Player.class, sc.getUserPrincipal().getName());
        return Response.ok(jsonify(player).toString()).build();
    }

    @Path("logout")
    @GET
    public Response logout(@Context HttpServletRequest servletRequest) {
        try {
            servletRequest.logout();
        } catch (ServletException e) {
            System.out.println(e.getCause());
        }
        return Response.ok().build();
    }

    @Path("create")
    @GET
    public String addTestUser(@QueryParam("email") String email, @QueryParam("pwd") String password, @QueryParam("username") String username) {
        if (em.find(Player.class, email.toLowerCase()) != null) {
            return "this user already exist.";
        }
        Player player = Player.CreateNewPlayer(email.toLowerCase(), hasher.generate(password.toCharArray()), username);
        if (player == null) {
            return "invalid params";
        }
        em.persist(player);
        System.out.println(player);
        return "new user created";
    }

    @Path("search")
    @GET
    public Response searchPlayers(@QueryParam("username") String username) {
        TypedQuery<Player> q = em.createQuery("SELECT p FROM Player p WHERE LOWER(p.username) LIKE :param", Player.class);
        q.setParameter("param", "%" + username.trim().toLowerCase() + "%");
        List<Player> resultList = q.getResultList();
        if (resultList != null && !resultList.isEmpty()) {
            JSONArray playerResults = new JSONArray();
            for (Player p : resultList) {
                playerResults.put(jsonify(p));
            }
            return Response.ok(playerResults.toString()).build();
        }
        return Response.ok().build();
    }

    @Path("get-match-history")
    @GET
    public Response getMatchHistory(@QueryParam("email") String email) {
        Player player = em.find(Player.class, email);
        if (player == null) {
            return Response.noContent().build();
        }

        List<MatchResult> matches = player.getMatchHistory();
        Collections.sort(matches, new Comparator<MatchResult>() {
            @Override
            public int compare(MatchResult left, MatchResult right) {
                LocalDateTime leftDate = left.getMatch().getDateTime();
                LocalDateTime rightDate = right.getMatch().getDateTime();
                return leftDate.compareTo(rightDate);
            }
        });

        JSONArray jsonArray = new JSONArray();
        System.out.println(player.getMatchHistory().size());
        for (MatchResult mr : player.getMatchHistory()) {
            jsonArray.put(jsonify(mr));
        }

        return Response.ok(jsonArray.toString()).build();
    }

    @Path("set-profile-image")
    @RolesAllowed("user")
    @POST
    public Response setProfilePicture(InputStream inputStream, @QueryParam("type") String type) {
        System.out.println("Image upload started");
        Player player = em.find(Player.class, sc.getUserPrincipal().getName());
        if (player == null) {
            return Response.notModified().build();
        }
        String imageId = UUID.randomUUID().toString();
        String imagePath = Paths.get(getImagePath(), imageId + "." + type).toString();
        
        
        try {
            Files.copy(inputStream, Paths.get(imagePath));
            player.setProfilePicturePath(imagePath);
            em.merge(player);
            
        } catch (IOException ex) {
            Logger.getLogger(CompanionAppService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Image uploaded").build();
    }


    @Path("get-profile-image")
    @GET
    public Response getProfilePicture(@QueryParam("email") String email) {
        Player player = em.find(Player.class, email);
        if (player == null || player.getProfilePicturePath() == null || player.getProfilePicturePath().isEmpty()) {
            return getDefaultImage();
        }
        String filePath = player.getProfilePicturePath();
        File image = new File(filePath);
        if (image.exists()) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                ImageIO.write(ImageIO.read(image), filePath.substring(filePath.lastIndexOf(".") + 1), out);
                out.close();
                return Response.ok(out.toByteArray()).build();
            } catch (IOException ex) {
                Logger.getLogger(CompanionAppService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Response.ok("Default image exists.").build();
        }
        return getDefaultImage();
    }

    @Path("get-default-image")
    @GET
    public Response getDefaultImage() {
        String filePath = getDefaultImagePath();
        File image = new File(filePath);
        if (image.exists()) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                ImageIO.write(ImageIO.read(image), "jpg", out);
                out.close();
                return Response.ok(out.toByteArray()).build();
            } catch (IOException ex) {
                Logger.getLogger(CompanionAppService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Response.ok("Default image exists.").build();
        }
        return Response.ok("default image does not exist.").build();
    }

    @Path("get-player")
    @GET
    public Response getPlayer(@QueryParam("email") String email) {
        Player result = em.find(Player.class, email);
        if (result != null) {
            return Response.ok(jsonify(result).toString()).build();
        }
        return Response.noContent().build();
    }
    
    @Path("set-description")
    @RolesAllowed("user")
    @POST
    public Response setDescription(@QueryParam("desc") String description) {
        Player player = em.find(Player.class, sc.getUserPrincipal().getName());
        if(player != null){
            player.setDescription(description);
            em.merge(player);
            return Response.ok(description).build();
        }
        return Response.notModified().build();
    }

//    @Path("dummy")
//    @GET
//    public Response addDummyData() {
//
//        String[] testNames1 = {"Toby", "Mike", "Donald", "Dmitri", "Charlotte", "Maxwell", "Chad", "Bruno", "Sasha", "Tyler", "Brigsby", "Jared"};
//        String[] testNames2 = {"Mars", "Trump", "James", "Jackson", "Willburry", "Jones", "Bond", "Baron", "Chrysler", "Jobs", "Huckaby", "Leto"};
//
//        List<Player> newPlayers = new ArrayList<>();
//        for (int i = 0; i < 30; i++) {
//            int randomIndex1 = (int) (Math.random() * testNames1.length);
//            int randomIndex2 = (int) (Math.random() * testNames2.length);
//            String defaultPwd = "123";
//            Player newPlayer = Player.CreateNewPlayer("e" + i + "@d.com", hasher.generate(defaultPwd.toCharArray()), testNames1[randomIndex1] + " " + testNames2[randomIndex2]);
//            newPlayer.setHighestAlcohol((float) (Math.random() * 6));
//            newPlayer.setLongestDrift((float) (Math.random() * 50));
//            newPlayer.incrementNumberOfTakedowns((int) (Math.random() * 100));
//            newPlayer.incrementNumberOfTinderMatches((int) (Math.random() * 50));
//            newPlayer.incrementTimesOutOfBounds((int) (Math.random() * 250));
//            newPlayers.add(newPlayer);
//            em.persist(newPlayer);
//        }
//        int day = 1;
//        for (int i = 0; i < 40; i++) {
//            int numberOfLaps = (int) (Math.random() * 3) + 3;
//            int mapIndex = (int) (Math.random() * 4);
//            day = Math.random() > 0.5f ? day + 1 : day;
//            Match match = new Match(mapIndex, numberOfLaps, LocalDateTime.of(2018, 8, day, (int) (Math.random() * 24), (int) (Math.random() * 60), (int) (Math.random() * 60)));
//            Set<MatchResult> results = new HashSet<>();
//            Set<Player> participants = new HashSet<>();
//            for (int j = 0; j < 10; j++) {
//                Player randomPlayer = null;
//                while (randomPlayer == null || participants.contains(randomPlayer)) {
//                    int randomIndex = (int) (Math.random() * newPlayers.size());
//                    randomPlayer = newPlayers.get(randomIndex);
//                }
//                participants.add(randomPlayer);
//                MatchResult.PlayableCharacter randomCharacter = MatchResult.PlayableCharacter.values()[(int) (Math.random() * MatchResult.PlayableCharacter.values().length)];
//                MatchResult playerResult = new MatchResult(match, (float) (Math.random() * numberOfLaps * 50000 + 50000), randomCharacter);
//                playerResult.setUpPlayer(randomPlayer);
//                results.add(playerResult);
//                em.persist(playerResult);
//                em.merge(randomPlayer);
//
//            }
//            match.setPlayerResults(results);
//            computeRankDistribution(match);
//            em.persist(match);
//        }
//
//        return Response.ok("dummydata created").build();
//    }
//
//    private void computeRankDistribution(Match match) {
//        ArrayList<MatchResult> playerPlaceInRace = new ArrayList<>(match.getPlayerResults());
//        Collections.sort(playerPlaceInRace, new Comparator<MatchResult>() {
//            @Override
//            public int compare(MatchResult r1, MatchResult r2) {
//                return r1.getLapTime() < r2.getLapTime() ? -1 : (r1.getLapTime() > r2.getLapTime()) ? 1 : 0;
//            }
//        });
//
//        for (int i = 0; i < playerPlaceInRace.size(); i++) {
//            MatchResult currentMatchResult = playerPlaceInRace.get(i);
//            float placeSign = (playerPlaceInRace.size() / 2f - i - 0.5f) / (playerPlaceInRace.size() / 2f);
//
//            float rankMultiplier = 1;//(float) /*Math.pow(*/ currentMatchResult.getPlayer().getRankPoints() / match.getAvearageRank()/*, 1.25f)*/;
//
//            float rankDelta = 50;
//
//            if (placeSign > 0) { //player gains rankpoints
//                rankDelta *= (placeSign / rankMultiplier);
//            }
//            else { //player loses rank points
//                rankDelta *= (placeSign * rankMultiplier);
//            }
//
//            int playerRankDelta = Math.round(rankDelta);
//            currentMatchResult.setPlaceInMatch(i);
//            currentMatchResult.setRankDelta(playerRankDelta);
//
////             if(i == 4) System.out.println("match player count: " + playerPlaceInRace.size() + "    ");
////            if(i == 4) System.out.println("fourth place rank: " + currentMatchResult.getPlayer().getRankPoints() + " vs avg: " + match.getAvearageRank() + "  rank multiplier: " + rankMultiplier + " place sign: " + placeSign + "   rankdelta: " + rankDelta);
//            em.merge(currentMatchResult);
//            em.merge(currentMatchResult.getPlayer());
//
//        }
//    }

    private JSONObject jsonify(MatchResult matchResult) {
        JSONObject jsnObj = new JSONObject();

        jsnObj.put("id", matchResult.getId());
        jsnObj.put("date", matchResult.getMatch().getDateTime().toString());
        jsnObj.put("player", matchResult.getPlayer().getId());
        jsnObj.put("character", (int) (MatchResult.PlayableCharacter.values().length * Math.random()));
        jsnObj.put("map", matchResult.getMatch().getRacingMapId());
        jsnObj.put("match", matchResult.getMatch().getId());
        jsnObj.put("time", matchResult.getLapTime());
        jsnObj.put("rank", matchResult.getRankDelta());
        jsnObj.put("place", matchResult.getPlaceInMatch());
        jsnObj.put("avg", matchResult.getMatch().getAvearageRank());

        return jsnObj;
    }

    private JSONObject jsonify(Player player) {
        JSONObject jsonObj = new JSONObject();

        jsonObj.put("email", player.getId());
        jsonObj.put("username", player.getUsername());
        jsonObj.put("alcohol", player.getHighestAlcohol());
        jsonObj.put("rankpoints", player.getRankPoints());
        jsonObj.put("drift", player.getLongestDrift());
        jsonObj.put("outofbounds", player.getTimesOutOfBounds());
        jsonObj.put("takedowns", player.getNumberOfTakedowns());
        jsonObj.put("tinder", player.getNumberOfTinderMatches());
        jsonObj.put("description", player.getDescription());

        JSONArray placeCount = new JSONArray();
        for (int place : player.getPlaceCount()) {
            placeCount.put(place);
        }
        jsonObj.put("place", placeCount);
        return jsonObj;
    }

    

}
