package marcusbj.gamecompanionapp.utillity

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import marcusbj.gamecompanionapp.data.Player
import marcusbj.gamecompanionapp.data.PlayerResult
import okhttp3.MultipartBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet
//import jdk.nashorn.internal.runtime.ScriptingFunctions.readLine
import android.R.attr.keySet
//import com.sun.xml.internal.ws.streaming.XMLStreamWriterUtil.getOutputStream
import java.io.*
import kotlin.collections.HashMap
import android.os.Build
import android.R.attr.tag
import android.util.Log
import java.net.NetworkInterface.getNetworkInterfaces


class NetworkUtils {
    companion object {
        private val BASE_URL: String
        private val boundary = "*****" + System.currentTimeMillis() + "*****"
        private const val CRLF = "\r\n";
        private val mCookieManager: CookieManager = CookieManager()

        init {
            CookieHandler.setDefault(mCookieManager)
            val thisIp = "158.38.79.100"
            println(thisIp)
            BASE_URL = if(isEmulator()) "http://10.0.2.2:8080/CompanionAppServer/api/stats" else "http://$thisIp:8080/CompanionAppServer/api/stats"
        }

        fun setProfileImage(imageUri: Uri): String? {
            println("sending image")
            val imageFile = File(imageUri.path)
            val url = URL(
                uriBuilder().appendPath("set-profile-image").appendQueryParameter(
                    "type",
                    imageFile.extension
                ).toString()
            )

            val urlConnection = url.openConnection() as HttpURLConnection
            try {

//                urlConnection.setRequestProperty("Content-Type", "mediatype/image")

                urlConnection.requestMethod = "POST"
//                    urlConnection.doOutput = true
                val outputStream = DataOutputStream(urlConnection.outputStream)
                outputStream.write(imageFile.readBytes())
                outputStream.flush()
                outputStream.close()
                println("sent image")

                val inStream = urlConnection.inputStream

                val scanner = Scanner(inStream)
                scanner.useDelimiter("\\A")
                val response = scanner.next()
                println("---------response code---------" + urlConnection.responseCode)
                println(response)
                return response
            } catch (e: java.lang.Exception) {

            } finally {

                urlConnection.disconnect()
            }

            return null

        }

        fun getPlayerProfileImagePath(email: String): URL {
            return URL(uriBuilder().appendPath("get-profile-image").appendQueryParameter("email", email).toString())
        }

        fun searchPlayer(query: String?): Set<Player>? {
            println(query)
            if (query.isNullOrBlank()) {
                return null
            }
            val response = getResponseFromHttpUrl(
                URL(
                    uriBuilder().appendPath("search").appendQueryParameter(
                        "username",
                        query
                    ).toString()
                )
            )
            if (response.isNullOrBlank()) {
                return null
            }
            val jsonResult = JSONArray(response)
            if (jsonResult.length() == 0) {
                return null
            }
            val playerSet = HashSet<Player>()
            for (i in 0 until jsonResult.length()) {
                playerSet.add(Player.deJsonify(jsonResult[i] as JSONObject))
            }

            return playerSet
        }

        fun loginUser(email: String, password: String): Player? {
            Authenticator.setDefault(object : Authenticator() {
                override fun getPasswordAuthentication(): PasswordAuthentication {
                    return PasswordAuthentication(email, password.toCharArray())
                }
            })
            val response = getResponseFromHttpUrl(
                URL(
                    uriBuilder().appendPath("login").toString()
                )
            )
            if (response.isNullOrBlank()) {
                return null
            }
            return try {
                Player.deJsonify(JSONObject(response))
            } catch (e: JSONException) {
                null
            }

        }

        fun getPlayerByEmail(email: String): Player? {
            val response = getResponseFromHttpUrl(
                URL(
                    uriBuilder().appendPath("get-player").appendQueryParameter("email", email).toString()
                )
            )
            if (response.isNullOrBlank()) {
                return null
            }
            return Player.deJsonify(JSONObject(response))
        }

        fun getMatchHistory(email: String): List<PlayerResult>? {
            val response = getResponseFromHttpUrl(
                URL(
                    uriBuilder().appendPath("get-match-history").appendQueryParameter("email", email).toString()
                )
            )
            if (response.isNullOrBlank()) {
                return null
            }
            val jsonArray = JSONArray(response)
            val playerResults = ArrayList<PlayerResult>()
            for (i in 0 until jsonArray.length()) {
                playerResults.add(PlayerResult.deJsonify(jsonArray[i] as JSONObject))
            }
            return playerResults
        }

//        fun getImageResponseFromHttpUrl(url: URL, postString: String = ""): ByteArray? {
//            val urlConnection = url.openConnection() as HttpURLConnection
//            try {
//                val inStream = urlConnection.inputStream
//
//                val scanner = ByteArrayInputStream()
//                scanner.useDelimiter("\\A")
//                val response = scanner.next()
//                println("---------response code---------" + urlConnection.responseCode)
//                println(response)
//                return response
//            }
//        }

        fun getResponseFromHttpUrl(url: URL, postString: String = ""): String? {
            val urlConnection = url.openConnection() as HttpURLConnection
            try {

                if (!postString.isBlank()) {

                    try {
                        JSONObject(postString)
                        urlConnection.setRequestProperty("Content-Type", "application/json")
                        println("SENDING JSON STRING")
                    } catch (e: JSONException) {

                    }


                    println("Sending message $postString")
                    urlConnection.requestMethod = "POST"
//                    urlConnection.doOutput = true
                    val outputStream = OutputStreamWriter(urlConnection.outputStream)
                    outputStream.write(postString)
                    outputStream.flush()
                    outputStream.close()
                }


                val inStream = urlConnection.inputStream

                val scanner = Scanner(inStream)
                scanner.useDelimiter("\\A")
                val response = scanner.next()
                println("---------response code---------" + urlConnection.responseCode)
                println(response)
                return response
//                scanner.useDelimiter("\\A")
//
//                val hasInput = scanner.hasNext()
//                return if (hasInput) {
//                    scanner.next()
//                } else {
//                    null
//                }
            } catch (e: Exception) {
                println(e.message)
                return null
            } finally {
                urlConnection.disconnect()
            }
        }

        fun setUserDescription(description: String): String? {
            val response = getResponseFromHttpUrl(
                URL(
                    uriBuilder().appendPath("set-description").appendQueryParameter("desc", description).toString()
                )
                , description
            )
            if (response.isNullOrBlank()) {
                return null
            }
            return response
        }

        private fun uriBuilder(): Uri.Builder {
            return Uri.parse(BASE_URL).buildUpon()
        }

        fun multipartRequest(
            urlTo: String,
            parmas: Map<String, String>,
            filepath: String,
            filefield: String,
            fileMimeType: String
        ): String {
            var connection: HttpURLConnection?
            var outputStream: DataOutputStream?
            var inputStream: InputStream?

            val twoHyphens = "--"
            val boundary = "*****" + java.lang.Long.toString(System.currentTimeMillis()) + "*****"
            val lineEnd = "\r\n"

            var result = ""

            var bytesRead: Int
            var bytesAvailable: Int
            var bufferSize: Int
            val buffer: ByteArray
            val maxBufferSize = 1 * 1024 * 1024

            val q = filepath.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val idx = q.size - 1

            try {
                val file = File(filepath)
                val fileInputStream = FileInputStream(file)

                val url = URL(urlTo)
                connection = url.openConnection() as HttpURLConnection

                connection.doInput = true
                connection.doOutput = true
                connection.useCaches = false

                connection.requestMethod = "POST"
                connection.setRequestProperty("Connection", "Keep-Alive")
                connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0")
                connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=$boundary")

                outputStream = DataOutputStream(connection.outputStream)
                outputStream.writeBytes(twoHyphens + boundary + lineEnd)
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd)
                outputStream.writeBytes("Content-Type: $fileMimeType$lineEnd")
                outputStream.writeBytes("Content-Transfer-Encoding: binary$lineEnd")

                outputStream.writeBytes(lineEnd)

                bytesAvailable = fileInputStream.available()
                bufferSize = Math.min(bytesAvailable, maxBufferSize)
                buffer = ByteArray(bufferSize)

                bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                while (bytesRead > 0) {
                    outputStream.write(buffer, 0, bufferSize)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                }

                outputStream.writeBytes(lineEnd)

                // Upload POST Data
                val keys = parmas.keys.iterator()
                while (keys.hasNext()) {
                    val key = keys.next()
                    val value = parmas[key]

                    outputStream.writeBytes(twoHyphens + boundary + lineEnd)
                    outputStream.writeBytes("Content-Disposition: form-data; name=\"$key\"$lineEnd")
                    outputStream.writeBytes("Content-Type: text/plain$lineEnd")
                    outputStream.writeBytes(lineEnd)
                    outputStream.writeBytes(value)
                    outputStream.writeBytes(lineEnd)
                }

                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)


                inputStream = connection.inputStream

                val scanner = Scanner(inputStream)
                scanner.useDelimiter("\\A")


                result = scanner.next()

                fileInputStream.close()
                inputStream!!.close()
                outputStream.flush()
                outputStream.close()

                return result
            } catch (e: Exception) {
                println("Something went wrong")
            }
            return result
        }

        fun isEmulator(): Boolean {
            return (Build.FINGERPRINT.startsWith("generic")
                    || Build.FINGERPRINT.startsWith("unknown")
                    || Build.MODEL.contains("google_sdk")
                    || Build.MODEL.contains("Emulator")
                    || Build.MODEL.contains("Android SDK built for x86")
                    || Build.MANUFACTURER.contains("Genymotion")
                    || Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic")
                    || "google_sdk" == Build.PRODUCT)
        }



    }

}