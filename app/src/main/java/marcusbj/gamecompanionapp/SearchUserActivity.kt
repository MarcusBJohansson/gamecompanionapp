package marcusbj.gamecompanionapp

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_search_user.*
import marcusbj.gamecompanionapp.data.Player
import marcusbj.gamecompanionapp.utillity.NetworkUtils

class SearchUserActivity : AppCompatActivity(), UsersAdapter.UserViewClickHandler {

    override fun onClick(email: String) {
        intent = Intent(this, ViewUserActivity::class.java)
        intent.putExtra("email", email)
        startActivity(intent)
    }

    private lateinit var mRecyclerView: androidx.recyclerview.widget.RecyclerView
    lateinit var mAdapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_user)
        setSupportActionBar(toolbar)

        mRecyclerView = findViewById(R.id.search_recycler_view)
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        mAdapter = UsersAdapter(this)

        mRecyclerView.adapter = mAdapter

//        mAdapter.set_test_data()

        val query = intent.getStringExtra("query")
        println(query)
        SearchUserTask(mAdapter).execute(query)
    }

}

class SearchUserTask(val receiver: TaskResult) : AsyncTask<String, Void, Set<Player>?>() {
    override fun doInBackground(vararg params: String?): Set<Player>? {
        return NetworkUtils.searchPlayer(params[0])
    }

    override fun onPostExecute(result: Set<Player>?) {
        if (result.isNullOrEmpty()) {
            receiver.onResult(HashSet())
            return
        }
        receiver.onResult(result)
//        mAdapter.set_user_data(result)
    }

    interface TaskResult {
        fun onResult(players: Set<Player>)
    }

}
