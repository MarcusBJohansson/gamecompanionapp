package marcusbj.gamecompanionapp

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.match_result_item.view.*
import marcusbj.gamecompanionapp.data.PlayerResult
import java.util.concurrent.TimeUnit

class PlayerResultAdapter(val clickHandler: MatchResultClickHandler) :
    androidx.recyclerview.widget.RecyclerView.Adapter<PlayerResultAdapter.MatchResultViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MatchResultViewHolder {
        val listItemLayout = R.layout.match_result_item
        val inflater = LayoutInflater.from(viewGroup.context)

        val view = inflater.inflate(listItemLayout, viewGroup, false)
        return MatchResultViewHolder(view)
    }

    override fun getItemCount(): Int {
        return matchResults.size
    }

    override fun onBindViewHolder(viewHolder: MatchResultViewHolder, position: Int) {
        viewHolder.bind(matchResults[position])
    }

    fun setResultsData(data: List<PlayerResult>) {
        matchResults.clear()
        matchResults.addAll(data)
        notifyDataSetChanged()
    }

    private val matchResults = ArrayList<PlayerResult>()

    interface MatchResultClickHandler {
        fun onClick(id: Int)
    }

    inner class MatchResultViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

        lateinit var matchResult: PlayerResult

        override fun onClick(v: View?) {
            clickHandler.onClick(matchResult.id)
        }

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(matchResult: PlayerResult) {
            this.matchResult = matchResult
            updateCharacterImage()
            val rankDelta = matchResult.rankDelta
            var rankDeltaText: String
            if (rankDelta > 0) {
                itemView.result_rank_points.setTextColor(ContextCompat.getColor(itemView.context, R.color.rp_positive))
                rankDeltaText = "+$rankDelta RP"
            } else if (rankDelta < 0) {
                itemView.result_rank_points.setTextColor(ContextCompat.getColor(itemView.context, R.color.rp_negative))
                rankDeltaText = "$rankDelta RP"
            } else {
                itemView.result_rank_points.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.abc_tint_default
                    )
                )
                rankDeltaText = "0 RP"
            }
            itemView.result_rank_points.text = rankDeltaText
            itemView.result_map.text = matchResult.racingMap.name
            itemView.result_time.text = getTimeString(matchResult.lapTime)
            itemView.result_date.text = matchResult.date
            val avgRankString = "avg: " + matchResult.avgRank + " RP"
            itemView.result_avg.text = avgRankString
            updatePlaceText()
        }

        private fun updatePlaceText() {
            when (matchResult.place) {
                0 -> itemView.result_place.text = "1st"
                1 -> itemView.result_place.text = "2nd"
                2 -> itemView.result_place.text = "3rd"
                else -> {
                    val placeString = "" + (matchResult.place + 1) + "th"
                    itemView.result_place.text = placeString
                }
            }
        }

        private fun updateCharacterImage() {
            val image = itemView.result_character
            when (matchResult.character) {
                PlayerResult.PlayableCharacter.BEAR_NAR -> image.setImageResource(R.mipmap.ic_a5_f)
                PlayerResult.PlayableCharacter.STORMI_WEBSTER -> image.setImageResource(R.mipmap.ic_a1_f)
                PlayerResult.PlayableCharacter.STONE_ISLAND_CHEESE_KING -> image.setImageResource(R.mipmap.ic_a2_f)
                PlayerResult.PlayableCharacter.ESPEN_VAN_DANGO -> image.setImageResource(R.mipmap.ic_a3_f)
                PlayerResult.PlayableCharacter.KARAMBE -> image.setImageResource(R.mipmap.ic_a4_f)
                PlayerResult.PlayableCharacter.MARCY_BJ -> image.setImageResource(R.mipmap.ic_a6_f)
            }
        }

        private fun getTimeString(millis: Float): String {
            val minutes = TimeUnit.MILLISECONDS.toMinutes(millis.toLong())
            val seconds = TimeUnit.MILLISECONDS.toSeconds(millis.toLong()) % 60
            val milliseconds = TimeUnit.MILLISECONDS.toMillis(millis.toLong()) % 60
            return "Time: $minutes:$seconds.$milliseconds"

        }
    }
}