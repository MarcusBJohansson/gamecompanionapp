package marcusbj.gamecompanionapp

import android.annotation.TargetApi
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import marcusbj.gamecompanionapp.data.Player
import marcusbj.gamecompanionapp.utillity.NetworkUtils

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity() {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private var mAuthTask: UserLoginTask? = null
    private var mToast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // Set up the login form.
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        email_sign_in_button.setOnClickListener { attemptLogin() }

    }

    private fun search() {
        val queryText = email.text.toString()
        if (queryText.length < 1) {
            email.error = "This feild is required"
            return
        }
        val intent = Intent(this, SearchUserActivity::class.java)
        intent.putExtra("query", queryText)
        startActivity(intent)
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {
        if (password.text.isEmpty()){
            search()
            return
        }
        if (password.visibility == View.GONE) {
            password.visibility = View.VISIBLE
            password.requestFocus()
            return
        }

        if (mAuthTask != null) {
            return
        }

        // Reset errors.
        email.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the player entered one.
        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.error_field_required)
            focusView = email
            cancel = true
        } else if (!isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the player login attempt.
            showProgress(true)
            mAuthTask = UserLoginTask(emailStr, passwordStr)
            val loginResponse = mAuthTask!!.execute()
            if(loginResponse!= null){
//                showToast("login successful!")
//                val intent = Intent(this, ViewUserActivity::class.java)
//                intent.putExtra("logged-in", true)
//                intent.putExtra("email", )
            }else{
                showToast("login unsuccessful")
            }
        }
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 1
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        login_progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun showToast(text:String){
        mToast?.cancel()
        mToast = Toast.makeText(this, text, Toast.LENGTH_SHORT)
        mToast?.show()
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the player.
     */
    inner class UserLoginTask internal constructor(private val mEmail: String, private val mPassword: String) :
        AsyncTask<Void, Void, Player>() {

        override fun doInBackground(vararg params: Void): Player? {
            // TODO: attempt authentication against a network service.
            return NetworkUtils.loginUser(mEmail, mPassword)
        }

        override fun onPostExecute(success: Player?) {
            mAuthTask = null
            showProgress(false)

            if (success != null) {
                showToast("login successful!")
                val intent = Intent(this@LoginActivity, ViewUserActivity::class.java)
                intent.putExtra("logged-in", true)
                intent.putExtra("email", success.email)
                startActivity(intent)
            } else {
                password.error = getString(R.string.error_incorrect_password)
                password.requestFocus()
            }
        }

        override fun onCancelled() {
            mAuthTask = null
            showProgress(false)
        }
    }

    companion object {

        /**
         * Id to identity READ_CONTACTS permission request.
         */
        private val REQUEST_READ_CONTACTS = 0

        /**
         * A dummy authentication store containing known player names and passwords.
         * TODO: remove after connecting to a real authentication system.
         */
        private val DUMMY_CREDENTIALS = arrayOf("foo@example.com:hello", "bar@example.com:world")
    }
}
