package marcusbj.gamecompanionapp

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.os.Bundle
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.*
import android.webkit.RenderProcessGoneDetail
import android.widget.Toast
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.theartofdev.edmodo.cropper.CropImage

import kotlinx.android.synthetic.main.activity_view_user.*
import kotlinx.android.synthetic.main.fragment_add_user.*
import kotlinx.android.synthetic.main.fragment_view_user.*
import marcusbj.gamecompanionapp.data.Player
import marcusbj.gamecompanionapp.data.PlayerResult
import marcusbj.gamecompanionapp.utillity.NetworkUtils
import kotlin.collections.ArrayList
import android.R.attr.data
import android.net.Uri
import com.squareup.picasso.Picasso


class ViewUserActivity : AppCompatActivity(), AddPlayerTask.TaskResult {

    override fun onResult(player: Player) {
        addPlayer(player)
    }

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private lateinit var mSectionsPagerAdapter: SectionsPagerAdapter

    private val players = ArrayList<Player>()
    var loggedIn = false


    private fun addPlayer(player: Player) {
        players.add(player)
        mSectionsPagerAdapter.notifyDataSetChanged()
    }

    private fun removePlayer(player: Player) {
        val indexOfPlayer = players.indexOf(player)
        if (indexOfPlayer == -1) {
            return
        }
        mSectionsPagerAdapter.removeMatchHistory(indexOfPlayer)
        players.remove(player)
        mSectionsPagerAdapter.notifyDataSetChanged()
        if (container.currentItem > 0) {
            container.currentItem--
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_user)

        setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

        loggedIn = intent.getBooleanExtra("logged-in", false)

        val emailOfSelectedPlayer = intent.getStringExtra("email")
        AddPlayerTask(this).execute(emailOfSelectedPlayer)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_view_user, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        when (id) {
            R.id.menu_add_user -> container.currentItem = players.size
            R.id.menu_remove_user -> {
                if (container.currentItem == players.size || (loggedIn && container.currentItem == 0)) {
                    return false
                }
                removePlayer(players[container.currentItem])
            }
        }

        return super.onOptionsItemSelected(item)
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        val matchHistories = ArrayList<List<PlayerResult>>()
        var update = false

        override fun getItem(position: Int): Fragment {

            if (position == players.size) {
                return AddUserFragment.newInstance(this@ViewUserActivity)
            } else /*if(players.size > position)*/ {

                println("show: " + position + " size: " + matchHistories.size)

                val loggedIn = position == 0 && loggedIn
                // getItem is called to instantiate the fragment for the given page.
                // Return a UserViewFragment (defined as a static inner class below).
                if (matchHistories.size <= position) {
//                    val result = GetMatchHistory(position, this).execute(players[position].email).get()
//                    if (result != null) {
//                        matchHistories.add(position, result)
//                    }

                    return UserViewFragment.newInstance(position, players[position], this, null, loggedIn)
                }

                return UserViewFragment.newInstance(
                    position,
                    players[position],
                    this,
                    matchHistories[position],
                    loggedIn
                )
            }
//            return AddUserFragment()
        }

        override fun getCount(): Int {
            return players.size + 1
        }

        override fun notifyDataSetChanged() {
            update = true
            super.notifyDataSetChanged()
            update = false
        }

        override fun getItemPosition(`object`: Any): Int {
            if (update) {
                return PagerAdapter.POSITION_NONE
            }
            return super.getItemPosition(`object`)
        }

        fun removeMatchHistory(indexOfPlayer: Int) {
            if(matchHistories.size != this@ViewUserActivity.players.size){
                return
            }
            matchHistories.removeAt(indexOfPlayer)
        }

    }

    class AddUserFragment : Fragment(), UsersAdapter.UserViewClickHandler {

        lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
        lateinit var adapter: UsersAdapter
        lateinit var addPlayerReceiver: AddPlayerTask.TaskResult


        override fun onClick(email: String) {
            AddPlayerTask(addPlayerReceiver).execute(email)
//            println("clicked user")
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_add_user, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            recyclerView = fragment_search_user_recyclerview
            recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(view.context)
            adapter = UsersAdapter(this)
            recyclerView.adapter = adapter

            fragment_search_button.setOnClickListener {
                searchUser()
            }


        }

        fun searchUser() {
            val query = fragment_edit_text.text.toString().trim()
            if (query.isEmpty()) {
                fragment_edit_text.error = "search cannot be blank."
                return
            }
            SearchUserTask(adapter).execute(query)
        }

        companion object {
            fun newInstance(addUserReceiver: AddPlayerTask.TaskResult): AddUserFragment {
                val fragment = AddUserFragment()
                fragment.addPlayerReceiver = addUserReceiver
                return fragment
            }
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    class UserViewFragment : Fragment(), PlayerResultAdapter.MatchResultClickHandler, GetMatchHistory.TaskResult {

        override fun onResult(playerResults: List<PlayerResult>) {
            updateRankHistory(playerResults)
            parentAdapter.matchHistories.add(playerResults)
        }

        override fun onClick(id: Int) {
            println("It was clicked!!!")
        }

        private var position = -1

        val entries = ArrayList<PieEntry>()
        private val rankEntries = ArrayList<Entry>()
        lateinit var player: Player
        lateinit var mAdapter: PlayerResultAdapter
        lateinit var parentAdapter: SectionsPagerAdapter
        var loggedIn = false
        var matchHistory: List<PlayerResult>? = null


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_view_user, container, false)
        }

        private fun toggleDescriptionVisibility(view: View, isCurrentUser: Boolean) {

            view.visibility = if (!isCurrentUser) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//            view.section_label.text = getString(R.string.large_text)

            updatePieChart()
            updateRankSymbol()
            updateFunStatistics()
            profile_description.text = player.description
            user_description.setText(player.description)
            profileUsername.text = player.username

            if (loggedIn) {
                profile_background.background = ContextCompat.getDrawable(
                    activity!!.applicationContext,
                    R.drawable.profile_logged_in_background
                )
                profileUsername.setTextColor(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.white
                    )
                )
                toggleDescriptionVisibility(profile_description, false)
                toggleDescriptionVisibility(editable_user_layout, true)

                user_description.setOnTouchListener(object : View.OnTouchListener {
                    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                        when (event?.action) {
                            MotionEvent.ACTION_DOWN -> toggleDescriptionVisibility(edit_button, true)
                        }
                        return v?.onTouchEvent(event) ?: true
                    }
                })

                userProfilePicture.setOnClickListener {
                    CropImage.activity().setAspectRatio(1, 1).start(this.context!!, this)
//                    Picasso.get().load(NetworkUtils.getDefaultImageURL().toString())
                }

                user_description.setOnKeyListener { v, keyCode, event ->
                    run {
                        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                            toggleDescriptionVisibility(edit_button, false)
                            editable_user_layout.requestFocus()
                            user_description.setText(player.description)
                            return@run true
                        }
                        return@run false
                    }
                }

                edit_button.setOnClickListener {
                    run()
                    {
                        SetDescriptionTask(this).execute(user_description.text.toString())
                        editable_user_layout.requestFocus()
                        toggleDescriptionVisibility(edit_button, false)
                    }
                }
            }


//            val profileImage = player.imageUri
//            if (profileImage != null) {
//                userProfilePicture.setImageURI(profileImage)
//            }
            Picasso.get().load(NetworkUtils.getPlayerProfileImagePath(player.email).toString()).into(userProfilePicture)

            profile_match_history.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(view.context)
            mAdapter = PlayerResultAdapter(this)
            println("adapter: $mAdapter")
            profile_match_history.adapter = mAdapter
            if (matchHistory != null) {
                println(matchHistory?.size)
                updateMatchHistory(matchHistory!!)
            } else {
                GetMatchHistory(this, this).execute(player.email)
//                parentAdapter.getMatchHistory(position)
//                val result = GetMatchHistory().execute(player.email).get()
//                result ?: return
//                updateMatchHistory(result)
            }
        }

        fun updateRankHistory(rankHistory: List<PlayerResult>) {
            updateMatchHistory(rankHistory)
        }

        private fun updateMatchHistory(data: List<PlayerResult>) {
            mAdapter.setResultsData(data.reversed())
            updateRankChart(data)
        }

        private fun updateRankChart(matchHistory: List<PlayerResult>) {
            val lineChart = profile_rank_line_chart
            rankEntries.clear()

            var currentRankPoints = player.rankPoints
            var pos = matchHistory.size
            for (m in matchHistory.reversed()) {
                rankEntries.add(Entry(pos.toFloat(), currentRankPoints.toFloat()))
                pos--
                currentRankPoints -= m.rankDelta
            }


//            var currentRankPoints = player.rankPoints
//            for (i in matchHistory.size - 1 until 0) {
//
//                rankEntries.add(0, Entry(i.toFloat(), currentRankPoints.toFloat()))
////                rankEntries.add(Entry(i.toFloat(), i.toFloat() * i.toFloat()))
//                currentRankPoints -= matchHistory[i].rankDelta
//            }
//            println(rankEntries.size)
            val lineDataSet = LineDataSet(rankEntries.reversed(), "Rank Progression")
            lineDataSet.axisDependency = YAxis.AxisDependency.RIGHT
            lineChart.xAxis.isEnabled = false
            lineChart.axisLeft.isEnabled = false
            lineDataSet.isHighlightEnabled = false
//            lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
            lineDataSet.lineWidth = 1.5f
//            lineDataSet.cubicIntensity = 0.1f
            lineChart.data = LineData(lineDataSet)
            lineChart.invalidate()

        }

        private fun updateFunStatistics() {
            tinder_matches.text = player.tinderMatches.toString()
            out_of_bounds.text = player.timesFellOutOfMap.toString()
            val alcohol = "" + ((player.highestAlcoholConcentration * 100).toInt() / 100f) + " ‰"
            intoxication.text = alcohol
            val longDrift = "" + ((player.longestDrift * 1000).toInt() / 1000f) + "m"
            drift.text = longDrift
            takedowns.text = player.numberOfTakeDowns.toString()
            val rankPointsText = player.rankPoints.toString() + " RP"
            profileRankPoints.text = rankPointsText
        }

        private fun updateRankSymbol() {
            val image = profileRankBadge
            when (player.rank) {
                Rank.BRONZE -> image.setImageResource(R.mipmap.ic_rank_bronze)
                Rank.SILVER -> image.setImageResource(R.mipmap.ic_silver_badge)
                Rank.GOLD -> image.setImageResource(R.mipmap.ic_gold_badge)
                Rank.PLATINUM -> image.setImageResource(R.mipmap.ic_platinum_badge)
                Rank.DIAMOND -> image.setImageResource(R.mipmap.ic_diamond_badge)
                Rank.LEGEND -> image.setImageResource(R.mipmap.ic_legend_badge)
            }
        }

        private fun updatePieChart() {
            entries.clear()

            var totalMatches = 0
            for (place in player.placeCount) {
                totalMatches += place
            }

            val pieChart = profilePieChart
            var pieEntry: PieEntry
//            var percent = 100f
            for (i in 1..5) {
                when (i) {
                    1 -> pieEntry = PieEntry(player.placeCount[0].toFloat() / totalMatches, "1st")
                    2 -> pieEntry = PieEntry(player.placeCount[1].toFloat() / totalMatches, "2nd")
                    3 -> pieEntry = PieEntry(player.placeCount[2].toFloat() / totalMatches, "3rd")
                    4 -> {
                        val fourthFifth = (player.placeCount[3] + player.placeCount[4]).toFloat() / totalMatches
                        pieEntry = PieEntry(fourthFifth, "4th - 5th")
                    }
                    5 -> {
                        var rest = 0
                        for (j in 5 until player.placeCount.size) {
                            rest += player.placeCount[j]
                        }
                        pieEntry = PieEntry(rest.toFloat() / totalMatches, "6th - 10th")
                    }

                    else -> pieEntry = PieEntry(12f)
                }

                entries.add(pieEntry)

            }
            val pieDataSet = PieDataSet(entries, "Average placing")
            pieDataSet.valueTextSize = 12f
            pieDataSet.valueTextColor = R.color.chart_text_color
            pieDataSet.sliceSpace = 2f
            pieDataSet.setColors(
                intArrayOf(
                    R.color.first_place_gold_color,
                    R.color.second_place_silver_color,
                    R.color.third_place_bronze_color,
                    R.color.fourth_fifth_place_color,
                    R.color.six_tenth_place_color
                )
                , context
            )

            pieChart.setUsePercentValues(true)
            pieChart.holeRadius = 50f
            pieChart.transparentCircleRadius = 55f


            pieChart.data = PieData(pieDataSet)
            pieChart.invalidate()
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result = CropImage.getActivityResult(data)
                if (resultCode == RESULT_OK) {
                    val resultUri = result.uri
                    UploadImageTask().execute(resultUri)
//                    player.imageUri = resultUri
//                    userProfilePicture.setImageURI(resultUri)
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    val error = result.error
                }
            }
        }

        inner class UploadImageTask : AsyncTask<Uri, Void, String>() {
            override fun doInBackground(vararg params: Uri?): String {
                val imageUri = params[0]
                imageUri ?: return ""
                val result = NetworkUtils.setProfileImage(imageUri)
                return result ?: ""
            }

            override fun onPostExecute(result: String?) {
                val profilePicUrl = NetworkUtils.getPlayerProfileImagePath(player.email).toString()
                Picasso.get().invalidate(profilePicUrl)
                Picasso.get().load(profilePicUrl).into(userProfilePicture)
            }

        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(
                sectionNumber: Int,
                player: Player,
                parentAdapter: SectionsPagerAdapter,
                matchHistory: List<PlayerResult>?,
                loggedIn: Boolean
            ): UserViewFragment {
                val fragment = UserViewFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                fragment.position = sectionNumber
                fragment.player = player
                fragment.matchHistory = matchHistory
                fragment.parentAdapter = parentAdapter
                fragment.loggedIn = loggedIn
                return fragment
            }
        }
    }


}

class SetDescriptionTask(val viewFragment: ViewUserActivity.UserViewFragment) :
    AsyncTask<String, Void, String>() {
    override fun doInBackground(vararg params: String?): String {
        val desc = params[0]
        desc ?: return ""
        val result = NetworkUtils.setUserDescription(desc)
        result ?: return ""
        return result
    }

    override fun onPostExecute(result: String?) {
        if (!result.isNullOrBlank()) {
            viewFragment.user_description.setText(result)
            Toast.makeText(viewFragment.context, "description updated!", Toast.LENGTH_SHORT).show()
        }
    }

    interface TaskResult {
        fun onResult(playerResults: String)
    }
}


class GetMatchHistory(val receiver: TaskResult, val viewFragment: ViewUserActivity.UserViewFragment) :
    AsyncTask<String, Void, List<PlayerResult>>() {
    override fun onPreExecute() {
        toggleVisibility(viewFragment.progressBar, show = true)
        toggleVisibility(viewFragment.profilePieChart, show = false)

    }

    override fun doInBackground(vararg params: String?): List<PlayerResult>? {
        val email = params[0]
        email ?: return null
        return NetworkUtils.getMatchHistory(email)
    }

    override fun onPostExecute(result: List<PlayerResult>) {
        toggleVisibility(viewFragment.progressBar, show = false)
        toggleVisibility(viewFragment.profilePieChart, show = true)
        receiver.onResult(result)
    }

    private fun toggleVisibility(view: View, show: Boolean) {

        view.visibility = if (!show) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    interface TaskResult {
        fun onResult(playerResults: List<PlayerResult>)
    }
}

class AddPlayerTask(private val taskResult: TaskResult) : AsyncTask<String, Void, Player?>() {
    override fun doInBackground(vararg params: String?): Player? {
        val email = params[0]
        email ?: return null
        println("email = $email")
        return NetworkUtils.getPlayerByEmail(email)
    }

    override fun onPostExecute(result: Player?) {
        result ?: return
        taskResult.onResult(result)
//            activity.addPlayer(result)
    }

    interface TaskResult {
        fun onResult(player: Player)
    }

}

//class UploadImageTask():AsyncTask<Uri, Void, >(){
//
//}