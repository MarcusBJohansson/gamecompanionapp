package marcusbj.gamecompanionapp.data

import android.net.Uri
import android.os.AsyncTask
import marcusbj.gamecompanionapp.Rank
import marcusbj.gamecompanionapp.utillity.NetworkUtils
import org.json.JSONObject

class Player(
    val email: String,
    val username: String,
    val placeCount: Array<Int>,
    val longestDrift: Float,
    val timesFellOutOfMap: Int,
    val highestAlcoholConcentration: Float,
    val numberOfTakeDowns: Int,
    val tinderMatches: Int,
    val rankPoints: Int,
    var description: String = ""
) {

    var imageUri:Uri? = null
//    lateinit var matchHistory: List<PlayerResult>

    val rank: Rank
        get() {
            return when {
                rankPoints < 100 -> Rank.BRONZE
                rankPoints < 200 -> Rank.SILVER
                rankPoints < 300 -> Rank.GOLD
                rankPoints < 400 -> Rank.PLATINUM
                rankPoints < 500 -> Rank.DIAMOND
                rankPoints >= 500 -> Rank.LEGEND
                else -> Rank.BRONZE
            }
        }

//    fun checkMatchHistory():Boolean{
//        if(matchHistory != null){
//            return true
//        }
//        return false
//    }
//
//    inner class GetMatchHistoryTask:AsyncTask<String, Void, List<PlayerResult>?>(){
//        override fun doInBackground(vararg params: String?): List<PlayerResult>? {
//            return NetworkUtils.getMatchHistory(email)
//        }
//
//        override fun onPostExecute(result: List<PlayerResult>?) {
//            result?: return
//            matchHistory = result
//        }
//    }

    companion object {
        fun deJsonify(jsonObject: JSONObject): Player {

            val placeCountObj = ArrayList<Int>()
            val jsonArray = jsonObject.getJSONArray("place")
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    placeCountObj.add(jsonArray[i] as Int)
                }
            }

            return Player(
                jsonObject.getString("email"),
                jsonObject.getString("username"),
                placeCountObj.toTypedArray(),
                jsonObject.getDouble("drift").toFloat(),
                jsonObject.getInt("outofbounds"),
                jsonObject.getDouble("alcohol").toFloat(),
                jsonObject.getInt("takedowns"),
                jsonObject.getInt("tinder"),
                jsonObject.getInt("rankpoints"),
                jsonObject.getString("description")

            )
        }
    }


}