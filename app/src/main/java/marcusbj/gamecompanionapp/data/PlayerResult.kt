package marcusbj.gamecompanionapp.data

import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class PlayerResult(
    val id: Int,
    val date: String,
    val playerEmail: String,
    val character: PlayableCharacter,
    val racingMap: RacingMap,
    val match: Int,
    val lapTime: Float,
    val rankDelta: Int,
    val place: Int,
    val avgRank : Int
) {


    enum class RacingMap(val id: Int) {
        MILITARY_RAID(0),
        RED_FOREST(1),
        CIRCUS(2),
        FJORD_EXPRESS(3)
    }

    enum class PlayableCharacter {
        STORMI_WEBSTER,
        ESPEN_VAN_DANGO,
        MARCY_BJ,
        BEAR_NAR,
        KARAMBE,
        STONE_ISLAND_CHEESE_KING
    }

    companion object {
        fun deJsonify(jsonObject: JSONObject): PlayerResult {
            return PlayerResult(
                jsonObject.getInt("id"),
                jsonObject.getString("date").substring(0, 10),
                jsonObject.getString("player"),
                PlayableCharacter.values()[jsonObject.getInt("character")],
                RacingMap.values()[jsonObject.getInt("map")],
                jsonObject.getInt("match"),
                jsonObject.getDouble("time").toFloat(),
                jsonObject.getInt("rank"),
                jsonObject.getInt("place"),
                jsonObject.getDouble("avg").toInt()
            )
        }
    }

}
