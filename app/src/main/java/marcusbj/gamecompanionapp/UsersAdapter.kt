package marcusbj.gamecompanionapp

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import marcusbj.gamecompanionapp.data.Player

class UsersAdapter(val clickHandler: UserViewClickHandler) : androidx.recyclerview.widget.RecyclerView.Adapter<UsersAdapter.UserViewHolder>(), SearchUserTask.TaskResult {
    override fun onResult(players: Set<Player>) {
        setUserData(players)
    }

    private val testData = arrayOf(
        UserDataItem("somethin1@domain.com", "user1", Rank.BRONZE),
        UserDataItem("somethin2@domain.com", "user2", Rank.PLATINUM),
        UserDataItem("somethin3@domain.com", "user3", Rank.SILVER),
        UserDataItem("somethin4@domain.com", "user4", Rank.GOLD),
        UserDataItem("somethin5@domain.com", "user5", Rank.PLATINUM),
        UserDataItem("somethin6@domain.com", "user6", Rank.DIAMOND),
        UserDataItem("somethin7@domain.com", "user7", Rank.LEGEND)
    )

//    public fun set_test_data() {
//        players.clear()
//        for (u in testData) {
//            players.add(u)
//        }
//        notifyDataSetChanged()
//    }

    val players = ArrayList<UserDataItem>()

//    public fun setUserData(newData: JSONArray) {
//        players.clear()
//        for (i in 0 until newData.length()) {
//            val jsonObj = newData[i] as JSONObject
//            val rank = Rank.valueOf(jsonObj.getString("rank"))
//            val user = UserDataItem(jsonObj.getString("email"), jsonObj.getString("username"), rank)
//            players.add(user)
//        }
//        notifyDataSetChanged()
//    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun onBindViewHolder(viewHolder: UserViewHolder, position: Int) {
        viewHolder.bind(players[position].username, players[position].rank)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): UserViewHolder {
        val listItemLayout = R.layout.user_item
        val inflater = LayoutInflater.from(viewGroup.context)

        val view = inflater.inflate(listItemLayout, viewGroup, false)
        return UserViewHolder(view)
    }

    private fun setUserData(newData: Set<Player>) {
        players.clear()
        for(player in newData){
            players.add(UserDataItem(player.email, player.username, player.rank))
        }
        notifyDataSetChanged()
    }


    inner class UserViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            clickHandler.onClick(players[adapterPosition].email)
        }

        val text = itemView.findViewById<TextView>(R.id.item_username)!!
        val image = itemView.findViewById<ImageView>(R.id.rank_badge)!!

        fun bind(username: String, rank: Rank) {
            text.text = username

            when (rank) {
                Rank.BRONZE -> image.setImageResource(R.mipmap.ic_rank_bronze)
                Rank.SILVER -> image.setImageResource(R.mipmap.ic_silver_badge)
                Rank.GOLD -> image.setImageResource(R.mipmap.ic_gold_badge)
                Rank.PLATINUM -> image.setImageResource(R.mipmap.ic_platinum_badge)
                Rank.DIAMOND -> image.setImageResource(R.mipmap.ic_diamond_badge)
                Rank.LEGEND -> image.setImageResource(R.mipmap.ic_legend_badge)
            }
        }
    }

    interface UserViewClickHandler {
        fun onClick(email: String)
    }
}

data class UserDataItem(val email: String, val username: String, val rank: Rank)

enum class Rank {
    BRONZE,
    SILVER,
    GOLD,
    PLATINUM,
    DIAMOND,
    LEGEND
}